import { getMapRoundCounts, resetEventLogTables } from "./event_log";
import { initGameHooks, initPlayerCommands } from "./hooks";

interface CVar {
  command: string;
  default: string | number;
}

const mapStatsCVars: CVar[] = [
  { command: "map_stats_enabled", default: 1 },
  {
    command: "map_stats_min_players",
    default: 1,
  },
];

const initCVars = () => {
  mapStatsCVars.forEach((cvar) => {
    if (!ConVarExists(cvar.command)) {
      CreateConVar(cvar.command, cvar.default, "");
    }
  });

  const printCommand = "map_stats_print";
  const resetCommand = "reset_stats";

  concommand.Add(printCommand, () => {
    const counts = getMapRoundCounts();
    console.log("Map counts:");
    counts.forEach((m) => console.log(`${m.map_name}: ${m.count}`));
  });
  concommand.Add(resetCommand, () => {
    console.log("Warning! Event data has been destroyed");
    resetEventLogTables();
  });
};



initCVars();
initGameHooks();
initPlayerCommands();
