import { executeSQL, queryRow, SQLResult } from "./database";

export const setupEventLogTables = () => {
  logErrorAndContinue(
    executeSQL(
      `CREATE TABLE IF NOT EXISTS ttt_round_log (
         round_id INTEGER NOT NULL PRIMARY KEY,
         map_name TEXT NOT NULL,
         timestamp INTEGER NOT NULL,
         result TEXT,
         num_players INTEGER)`
    )
  );

  logErrorAndContinue(
    executeSQL(
      `CREATE TABLE IF NOT EXISTS ttt_kills_log (
        round_id INTEGER REFERENCES ttt_round_log(round_id),
        timestamp INTEGER NOT NULL,
        victim TEXT NOT NULL,
        attacker TEXT,
        entity TEXT NOT NULL)`
    )
  );
};

export const resetEventLogTables = () => {
  logErrorAndContinue(
    executeSQL(`
    DROP TABLE ttt_round_log
  `)
  );

  logErrorAndContinue(
    executeSQL(`
    DROP TABLE ttt_kills_log
  `)
  );

  setupEventLogTables();
};

export const createNewRound = (map: string, numPlayers: number) => {
  const roundId = generateNewRoundId();
  // const timestamp = os.time()
  const timestamp = 0;
  logErrorAndContinue(
    executeSQL(
      `INSERT INTO ttt_round_log (round_id, map_name, timestamp, num_players) VALUES ('${roundId}', '${map}', '${timestamp}', '${numPlayers}')`
    )
  );
  return roundId;
};

export const endRound = (roundId: number, result: string) => {
  return logErrorAndContinue(
    executeSQL(
      `UPDATE ttt_round_log SET result = '${result}' WHERE round_id = '${roundId}'`
    )
  );
};

export const getTotalRoundCount = (): number => {
  const { result, error } = executeSQL<{
    count: number;
  }>(`SELECT COUNT(*) AS count FROM ttt_round_log`);
  if (error) {
    console.error(error);
    return 0;
  }

  return result[0] ? result[0].count : 0;
};

const generateNewRoundId = () => {
  const query = `SELECT round_id FROM ttt_round_log ORDER BY round_id DESC LIMIT 1`;
  const max_round = queryRow<{ round_id: number }>(query);
  if (max_round && max_round?.result) {
    return max_round.result.round_id + 1;
  } else {
    return 0;
  }
};

export const addKill = (
  roundId: number,
  victim: string,
  entity: string,
  attacker?: string
) => {
  const timestamp = 0;
  logErrorAndContinue(
    executeSQL(
      `INSERT INTO ttt_kills_log VALUES ('${roundId}', '${timestamp}',  '${victim}', ${
        attacker ? `'${attacker}'` : "NULL"
      }, '${entity}')`
    )
  );
};

const logErrorAndContinue = (result: SQLResult<any>) => {
  if (result.error) {
    console.error(result.error);
  }
};

export const getMapRoundCounts = () => {
  const { error, result } = executeSQL<{
    map_name: string;
    count: number;
  }>(
    `SELECT map_name, COUNT(map_name) AS count from ttt_round_log WHERE result IS NOT NULL GROUP BY map_name`
  );

  if (error) {
    console.error(error);
    return [];
  }

  return result;
};

export const getWinCountsForMap = (mapName: string) => {
  const { error, result } = executeSQL<{
    result: string;
    count: number;
  }>(
    `SELECT result, COUNT(result) AS count from ttt_round_log WHERE result IS NOT NULL AND map_name = '${mapName}' GROUP BY result`
  );

  if (error) {
    console.error(error);
    return [];
  }

  return result;
};

export const getKillsForRound = (roundId: number) => {
  const { error, result } = executeSQL<{
    victim: string;
    entity: string;
    attacker?: string;
    timestamp: number;
  }>(
    `SELECT victim, entity, attacker, ttt_kills_log.timestamp
      FROM ttt_kills_log JOIN ttt_round_log ON ttt_kills_log.round_id = ttt_round_log.round_id
      WHERE result IS NOT NULL AND ttt_kills_log.round_id = '${roundId}'`
  );

  if (error) {
    console.error(error);
    return [];
  }

  return result;
};

export const getOverallWinStats = () => {
  const { error, result } = executeSQL<{
    result: string;
    win_rate: number;
  }>(
    "SELECT result, 100 * (CAST( COUNT(result) AS float) / (SELECT count(*) FROM ttt_round_log)) as win_rate FROM ttt_round_log GROUP BY result"
  );

  if (error) {
    console.error(error);
    return [];
  }

  return result;
};
