/** @noSelfInFile **/

import {
  getKillsForRound,
  getOverallWinStats,
  getWinCountsForMap,
} from "./event_log";
import { HUD_CHANNEL } from "./constants";

export type Printer = (text: string) => void;

export const chatPrinter: Printer = (text) =>
  PrintMessage(HUD_CHANNEL.HUD_PRINTTALK, text);

export const publishRoundKillStats = (roundId: number, printer: Printer) => {
  const kills = getKillsForRound(roundId);

  printer("Round Death Log:");
  kills.forEach(({ victim, entity, attacker }) =>
    printer(`${victim} was killed by ${attacker} using ${entity}`)
  );
};

export const printMapStatsCommand = (printer: Printer) => {
  const currentMap = game.GetMap();
  const counts = getWinCountsForMap(currentMap);
  printer(`Statistics for map ${currentMap}:`);
  counts.forEach((count) => printer(`${count.result}: ${count.count}`));
};

export const publishOverallWinStats = (printer: Printer) => {
  const counts = getOverallWinStats();

  printer("Overall win statistics:");
  counts.forEach(({ result, win_rate }) => {
    printer(`Team: ${result} - Win Rate: ${win_rate}`);
  });
};
