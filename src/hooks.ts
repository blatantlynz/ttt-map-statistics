/** @noSelfInFile **/

import {
  setupEventLogTables,
  createNewRound,
  endRound,
  addKill,
} from "./event_log";
import { HUD_CHANNEL, DAMAGE_TYPE } from "./constants";
import {
  publishRoundKillStats,
  publishOverallWinStats,
  printMapStatsCommand,
  Printer,
  chatPrinter,
} from "./chat-messages";

const isStatsEnabled = () => {
  return Object.keys(player.GetAll()).length > 2;
};

let activeRoundId: number | undefined;

const extractDeathType = (dmgInfo: CTakeDamageInfo): string => {
  const weapon = util.WeaponFromDamage(dmgInfo);
  if (weapon) {
    return weapon.GetPrintName();
  }

  return DAMAGE_TYPE[dmgInfo.GetDamageType()].toLowerCase();
};

export const initGameHooks = () => {
  hook.Add("InitPostEntity", "InitPostEntity", () => setupEventLogTables());

  hook.Add("TTTBeginRound", "map_stats", () => {
    // TODO: Work out if player.GetAll() counts spectators or not
    if (isStatsEnabled()) {
      activeRoundId = createNewRound(
        game.GetMap(),
        Object.keys(player.GetAll()).length
      );
    }
  });

  hook.Add("TTTEndRound", "map_stats", (result: string) => {
    // If enabled and we have more than the minimum number of players for stats tracking
    if (isStatsEnabled()) {
      console.log(`Saving round ${activeRoundId} stats. Result was ${result}`);
      if (!activeRoundId) {
        console.warn(
          "Round ended, but there was no active round. Not saving any statistics"
        );
        return;
      }
      endRound(activeRoundId, result);

      publishRoundKillStats(activeRoundId, (msg) =>
        PrintMessage(HUD_CHANNEL.HUD_PRINTTALK, msg)
      );

      activeRoundId = undefined;
    } else {
      console.log(
        "Not saving statistics because there were fewer than 2 players"
      );
    }
  });

  hook.Add(
    "DoPlayerDeath",
    "onDeath",
    (victim: Player, attacker: Player, dmgInfo: CTakeDamageInfo) => {
      if (activeRoundId) {
        addKill(
          activeRoundId,
          victim.GetName(),
          extractDeathType(dmgInfo),
          attacker.GetName()
        );
      }
    }
  );

  PrintMessage(HUD_CHANNEL.HUD_PRINTTALK, "hai");
};

export const initPlayerCommands = () => {
  const registeredCommands = [
    registerChatCommand("!stats", printMapStatsCommand),
    registerChatCommand("!winrate", publishOverallWinStats),
  ];

  // This doesn't match the argument order in the documentation, but seems to be correct
  // https://wiki.facepunch.com/gmod/GM:PlayerSay
  hook.Add<(ply: Player, text: string, isTeam: boolean) => void>(
    "PlayerSay",
    "stats",
    (ply, text, isTeam) => {
      registeredCommands.forEach((command) => command(text, chatPrinter));
    }
  );
};

const registerChatCommand = (
  text: string,
  callback: (printer: Printer) => void
) => {
  return (chatMessage: string, printer: Printer) => {
    if (chatMessage.startsWith(text)) {
      callback(printer);
    }
  };
};
