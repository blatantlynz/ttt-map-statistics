/** @noSelfInFile */

export interface SQLResult<T> {
  result: T;
  error?: string;
}

export const executeSQL = <T>(query: string): SQLResult<T[]> => {
  const result: T[] | boolean | undefined = sql.Query(query);

  if (result === false || result === true) {
    return {
      result: [],
      error: `SQL Statement: ${query} failed with error: ${sql.LastError()}`,
    };
  }

  return {
    result: result || [],
  };
};

export const queryRow = <T>(query: string): Partial<SQLResult<T>> => {
  const result: T | boolean | undefined = sql.QueryRow(query);

  if (result === false || result === true) {
    return {
      error: `SQL Statement: ${query} failed with error: ${sql.LastError()}`,
    };
  }

  return {
    result,
  };
};
