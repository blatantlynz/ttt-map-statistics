#!/bin/sh

# Copies files to the local copy of garrysmod. Useful for local development

cd "$(dirname "$0")/.."

export BUILD_ARTIFACT=dist/ttt-round-stats.gma

~/Library/Application\ Support/Steam/steamapps/common/GarrysMod/GarrysMod_Signed.app/Contents/MacOS/gmpublish update -id 2066491338 -addon $BUILD_ARTIFACT -icon image.jpg