#!/bin/sh

cd "$(dirname "$0")/.."

busted test/ --pattern=_test --lpath="./mock/?.lua;./src/lua/autorun/server/?.lua;./src/lua/includes/modules/?.lua;"