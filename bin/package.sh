#!/bin/sh

# Copies files to the local copy of garrysmod. Useful for local development

cd "$(dirname "$0")/.."

export BUILD_DIR=dist/ttt-round-stats

yarn build

cp addon.json $BUILD_DIR

~/Library/Application\ Support/Steam/steamapps/common/GarrysMod/GarrysMod_Signed.app/Contents/MacOS/gmad create -folder $BUILD_DIR