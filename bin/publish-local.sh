#!/bin/sh

# Copies files to the local copy of garrysmod. Useful for local development

cd "$(dirname "$0")/.."

set -ex

yarn build
cp -r dist/ttt-round-stats/ ~/Library/Application\ Support/Steam/steamapps/common/GarrysMod/garrysMod