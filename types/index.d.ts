/** @noSelfInFile */

/// Enitiy Types

declare interface Entity {
  GetName: () => string;
}

declare interface Player extends Entity {}

declare interface CTakeDamageInfo {
  GetInflictor: () => Entity;
  GetDamageType: () => number;
}

declare interface Weapon extends Entity {
  GetPrintName: () => string;
}

/// Global functions

declare function ConVarExists(name: string): boolean;
declare function CreateConVar(
  name: string,
  value: string | number,
  helptext: string
): boolean;

declare function PrintMessage(channel: number, text: string): void;

/// Global Objects

/** @noSelf **/
declare interface ConCommandGlobal {
  Add: (
    name: string,
    callback?: (
      player: PlayerGlobal,
      command: string,
      arguments: object,
      argumentString: string
    ) => void,
    autoComplete?: (this: void) => void,
    helpText?: string,
    flags?: number
  ) => void;
}

declare const concommand: ConCommandGlobal;

/** @noSelf **/
declare interface SQLGlobal {
  Query: <T>(query: String) => undefined | boolean | T;
  QueryRow: <T>(query: String) => undefined | boolean | T;
  LastError: () => string;
}

declare const sql: SQLGlobal;

/** @noSelf **/
declare interface TableGlobal {
  ToString: (table: object) => string;
}

declare const table: TableGlobal;

type Foo = "TTTEndRound";

/** @noSelf **/
declare interface HookGlobal {
  // hook.Add("TTTEndRound", "map_stats",
  // Add: (eventName: Foo, identifier: any, callback: () => void);
  Add: <F extends Function>(
    eventName: string,
    identifier: any,
    callback: F
  ) => void;
}

declare const hook: HookGlobal;

/** @noSelf **/
declare interface GameGlobal {
  GetMap: () => string;
}

declare const game: GameGlobal;

/** @noSelf **/
declare interface PlayerGlobal {
  GetAll: () => { [index: number]: Player };
}

declare const player: PlayerGlobal;
