/** @noSelf */
declare interface Utils {
  WeaponFromDamage: (dmgInfo: CTakeDamageInfo) => Weapon | undefined;
}

declare const util: Utils;
