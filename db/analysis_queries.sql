select result, 100 * (cast( count(result) as float) / (select count(*) from ttt_round_log)) as win_rate from ttt_round_log group by result;

select entity, count(entity) as count  from ttt_kills_log group by entity order by count desc;
select attacker, count(attacker) as count  from ttt_kills_log group by attacker order by count desc;

select map_name, count(map_name), result, count(result) as count  from ttt_round_log group by map_name, result order by map_name desc;
